# Etcd - etcd key-value storage 

## How to build images
```sh
docker build -t docker-repos:8123/yale/ire-etcd:{{ $tag }}
docker push docker-repos:8123/yale/ire-etcd:{{ $tag }}
```

## How to deploy on kubernetes
```sh
cd development
kubectl create -f svc.yaml    # create the headless service
kubectl create -f etcd.yaml   # create the statefulSet of etcd
```
