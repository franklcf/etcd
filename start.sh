#!/bin/sh
HOSTNAME=$(hostname)
SET_NAME=${SET_NAME:-"etcd"}
ETCD_DATA_DIR=${ETCD_DATA_DIR:-"/var/run/etcd/default.etcd"}
ETCD_HEARTBEAT_INTERVAL=${ETCD_HEARTBEAT_INTERVAL:-"100"}
ETCD_ELECTION_TIMEOUT=${ETCD_ELECTION_TIMEOUT:-"1000"}
ETCD_MAX_SNAPSHOTS=${ETCD_MAX_SNAPSHOTS:-"5"}
ETCD_MAX_WALS=${ETCD_MAX_WALS:-"5"}
ETCD_MAX_TXN_OPS=${ETCD_MAX_TXN_OPS:-"128"}
ETCD_MAX_REQUEST_BYTES=${ETCD_MAX_REQUEST_BYTES:-"1572864"}
ETCD_GRPC_KEEPALIVE_MIN_TIME=${ETCD_GRPC_KEEPALIVE_MIN_TIME:-"5s"}
ETCD_GRPC_KEEPALIVE_INTERVAL=${ETCD_GRPC_KEEPALIVE_INTERVAL:-"2h"}
ETCD_GRPC_KEEPALIVE_TIMEOUT=${ETCD_GRPC_KEEPALIVE_TIMEOUT:-"20s"}
ETCD_INITIAL_CLUSTER_TOKEN=${ETCD_INITIAL_CLUSTER_TOKEN:-"etcd-cluster"}


collect_member() {
    while ! etcdctl member list &>/dev/null; do sleep 2; done
    etcdctl member list | grep http://${HOSTNAME}.${SET_NAME}:2380 | cut -d':' -f1 | cut -d'[' -f1 | cut -d',' -f1 > /var/run/etcd/member_id
    exit 0
}

eps() {
    EPS=""
    for i in $(seq 0 $((${INITIAL_CLUSTER_SIZE} - 1))); do
        EPS="${EPS}${EPS:+,}http://${SET_NAME}-${i}.${SET_NAME}:2379"
    done
    echo ${EPS}
}

member_hash() {
    etcdctl member list | grep http://${HOSTNAME}.${SET_NAME}:2380 | cut -d':' -f1 | cut -d'[' -f1 | cut -d',' -f1
}

if [ -e ${ETCD_DATA_DIR} ]; then
    echo "Re-joining etcd member.."
    sleep 10
    member_id=$(cat /var/run/etcd/member_id)
    POD_IP=$(hostname -i)
    ETCDCTL_ENDPOINTS=$(eps)
    #etcdctl member update ${member_id} --peer-urls="http://${HOSTNAME}.${SET_NAME}:2380"
    exec etcd --name ${HOSTNAME} \
        --listen-peer-urls http://${POD_IP}:2380 \
        --listen-client-urls http://${POD_IP}:2379,http://127.0.0.1:2379 \
        --advertise-client-urls http://${POD_IP}:2379 \
        --data-dir ${ETCD_DATA_DIR} \
        --heartbeat-interval ${ETCD_HEARTBEAT_INTERVAL} \
        --election-timeout ${ETCD_ELECTION_TIMEOUT} \
        --max-snapshots ${ETCD_MAX_SNAPSHOTS} \
        --max-wals ${ETCD_MAX_WALS} \
        --max-txn-ops ${ETCD_MAX_TXN_OPS} \
        --max-request-bytes ${ETCD_MAX_REQUEST_BYTES} \
        --grpc-keepalive-min-time ${ETCD_GRPC_KEEPALIVE_MIN_TIME} \
        --grpc-keepalive-interval ${ETCD_GRPC_KEEPALIVE_INTERVAL} \
        --grpc-keepalive-timeout ${ETCD_GRPC_KEEPALIVE_TIMEOUT}

fi

SET_ID=${HOSTNAME:5:${#HOSTNAME}}

if [ "${SET_ID}" -ge ${INITIAL_CLUSTER_SIZE} ]; then
    export ETCDCTL_ENDPOINTS=$(eps)
    MEMBER_HASH=$(member_hash)

    if [ -n "${MEMBER_HASH}" ]; then
        etcdctl member remove ${MEMBER_HASH}
    fi

    echo "Adding new member"
    etcdctl member add ${HOSTNAME} --peer-urls="http://${HOSTNAME}.${SET_NAME}:2380" | grep "^ETCD_" > /var/run/etcd/new_member_envs

    if [ $? -ne 0 ]; then
        echo "Exiting"
        rm -f /var/run/etcd/new_member_envs
        exit 1
    fi

    cat /var/run/etcd/new_member_envs
    source /var/run/etcd/new_member_envs
    collect_member &
    POD_IP=$(hostname -i)
    echo "Waiting for DNS record setup.."
    sleep 20
    echo "Setting env config.."
    sleep 10
    exec etcd --name ${HOSTNAME} \
                    --listen-peer-urls http://${POD_IP}:2380 \
                    --listen-client-urls http://${POD_IP}:2379,http://127.0.0.1:2379 \
                    --advertise-client-urls http://${POD_IP}:2379 \
                    --data-dir ${ETCD_DATA_DIR} \
                    --heartbeat-interval ${ETCD_HEARTBEAT_INTERVAL} \
                    --election-timeout ${ETCD_ELECTION_TIMEOUT} \
                    --max-snapshots ${ETCD_MAX_SNAPSHOTS} \
                    --max-wals ${ETCD_MAX_WALS} \
                    --max-txn-ops ${ETCD_MAX_TXN_OPS} \
                    --max-request-bytes ${ETCD_MAX_REQUEST_BYTES} \
                    --grpc-keepalive-min-time ${ETCD_GRPC_KEEPALIVE_MIN_TIME} \
                    --grpc-keepalive-interval ${ETCD_GRPC_KEEPALIVE_INTERVAL} \
                    --grpc-keepalive-timeout ${ETCD_GRPC_KEEPALIVE_TIMEOUT} \
                    --initial-cluster-token ${ETCD_INITIAL_CLUSTER_TOKEN} \
                    --initial-advertise-peer-urls ${ETCD_INITIAL_ADVERTISE_PEER_URLS} \
                    --initial-cluster ${ETCD_INITIAL_CLUSTER} \
                    --initial-cluster-state ${ETCD_INITIAL_CLUSTER_STATE}
fi

for i in $(seq 0 $((${INITIAL_CLUSTER_SIZE} - 1))); do
    while true; do
        echo "Waiting for ${SET_NAME}-${i}.${SET_NAME} to come up"
        ping -W 1 -c 1 ${SET_NAME}-${i}.${SET_NAME}.${NAMESPACE}.svc.cluster.local > /dev/null && break
        sleep 1s
    done
done

PEERS=""

for i in $(seq 0 $((${INITIAL_CLUSTER_SIZE} - 1))); do
    PEERS="${PEERS}${PEERS:+,}${SET_NAME}-${i}=http://${SET_NAME}-${i}.${SET_NAME}:2380"
done

collect_member &
POD_IP=$(hostname -i)

#
echo "Waiting for all cluster node running.."
sleep 10

exec etcd --name ${HOSTNAME} \
    --initial-advertise-peer-urls http://${POD_IP}:2380 \
    --listen-peer-urls http://${POD_IP}:2380 \
    --listen-client-urls http://${POD_IP}:2379,http://127.0.0.1:2379 \
    --advertise-client-urls http://${POD_IP}:2379 \
    --initial-cluster-token ${ETCD_INITIAL_CLUSTER_TOKEN} \
    --initial-cluster ${PEERS} \
    --initial-cluster-state new \
    --data-dir ${ETCD_DATA_DIR} \
    --heartbeat-interval ${ETCD_HEARTBEAT_INTERVAL} \
    --election-timeout ${ETCD_ELECTION_TIMEOUT} \
    --max-snapshots ${ETCD_MAX_SNAPSHOTS} \
    --max-wals ${ETCD_MAX_WALS} \
    --max-txn-ops ${ETCD_MAX_TXN_OPS} \
    --max-request-bytes ${ETCD_MAX_REQUEST_BYTES} \
    --grpc-keepalive-min-time ${ETCD_GRPC_KEEPALIVE_MIN_TIME} \
    --grpc-keepalive-interval ${ETCD_GRPC_KEEPALIVE_INTERVAL} \
    --grpc-keepalive-timeout ${ETCD_GRPC_KEEPALIVE_TIMEOUT}
