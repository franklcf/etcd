FROM quay.io/coreos/etcd:v3.3.17

COPY start.sh preStop.sh /

RUN chmod +x /*.sh \
    && apk add vim busybox-extras bash

