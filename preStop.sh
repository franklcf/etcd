#!/bin/sh
EPS=""

for i in $(seq 0 $((${INITIAL_CLUSTER_SIZE} - 1))); do
    EPS="${EPS}${EPS:+,}http://${SET_NAME}-${i}.${SET_NAME}:2379"
done

HOSTNAME=$(hostname)

member_hash() {
    etcdctl member list | grep http://${HOSTNAME}.${SET_NAME}:2380 | cut -d':' -f1 | cut -d'[' -f1 | cut -d',' -f1
}

SET_ID=${HOSTNAME:5:${#HOSTNAME}}

if [ "${SET_ID}" -ge ${INITIAL_CLUSTER_SIZE} ]; then
  echo "Removing ${HOSTNAME} from etcd cluster"
  export ETCDCTL_ENDPOINTS=${EPS}
  etcdctl member remove $(member_hash)
  if [ $? -eq 0 ]; then
    rm -rf /var/run/etcd/*
    echo "Removing etcd data.."
  fi
fi
